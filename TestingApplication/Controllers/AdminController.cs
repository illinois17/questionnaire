﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestingApplication.Models;

using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestingApplication.Controllers
{

    public class AdminController : Controller
    {
        IRepository<Test> dbTest;
        IRepository<Question> dbQuestion;
        IRepository<Choice> dbChoices;
        IRepository<User> dbUser;
        MyDbContext db;
        public static int questTestId;
        public static int questIdPerem;
        public static int questionNumber;      

        public AdminController(MyDbContext context)
        {
            db = context;
            dbUser = new UserRepository(context);
            dbTest = new TestRepository(context);
            dbQuestion = new QuestionRepository(context);
            dbChoices = new ChoiseRepository(context);
        }

        // GET: /<controller>/        

        public IActionResult AdminView()
        {
            return View();
        }
        //Контроллеры создания теста
        //Создание теста
        public ActionResult CreateTest()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTest(Test test)
        {
            if (ModelState.IsValid)
            {
                test.AuthorID = 1;
                questionNumber = 0;
                questTestId = dbTest.Create(test);
                return RedirectToAction("CreateQuestion");
            }
            else
            {
                ModelState.AddModelError("", "Некорректные данные");
            }
            return View(test);
        }
        //Контроллеры создания текста вопросов 
        public ActionResult CreateQuestion()
        {
            questionNumber++;
            ViewBag.NumQuest = questionNumber;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateQuestion(Question quest)
        {
            if (ModelState.IsValid)
            {                
                quest.TestID = questTestId;
                quest.QuestionNumber = questionNumber;
                ViewBag.NumQuest = questionNumber;                
                foreach (Choice choice in quest.Choices)
                {
                    
                    choice.QuestionNumber = questionNumber;
                    choice.TestID = questTestId;                    
                }
                dbQuestion.Create(quest);
                return RedirectToAction("CreateQuestion");
            }
            else
            {
                ModelState.AddModelError("", "Некорректные данные");
            }
            return View(quest);
        }
            
        //TestList
        public IActionResult TestList()
        {
            return View(dbTest.GetList());
        }

        // GET: TestList/Edit/5
        [HttpGet]
        public ActionResult EditTest(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Test test = db.Tests.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }
            return View();
        }

        [HttpPost]
        public ActionResult EditTest(Test test)
        {
            db.Entry(test).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("AdminView");
        }
        // GET: TestList/Delete/5

        public ActionResult DeleteTest(int id)
        {
            Test test = db.Tests.Find(id);
            if (test != null)
            {
                db.Tests.Remove(test);
                db.SaveChanges();
            }
            return View();
        }

        [HttpGet]
        public ActionResult DeleteTest(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Test test = db.Tests.Find(id);
            if (test == null)
            {
                return HttpNotFound();
            }
            return View();
        }
        // Удаление теста и вопросов к нему 
        [HttpPost, ActionName("DeleteTest")]
        public ActionResult DeleteTestConfirmed(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Test test = db.Tests.Find(id);            
            var questDelete = db.Questions.Where(x => x.TestID == id);
            var choiceDelete = db.Choices.Where(x => x.TestID == id);
            if (test == null)
            {
                return HttpNotFound();
            }
            db.Tests.Remove(test);
            foreach (var c in questDelete){ db.Questions.Remove(c);}    //оставить табуляцию?
            foreach (var x in choiceDelete){ db.Choices.Remove(x); }
            db.SaveChanges();
            return RedirectToAction("AdminView");
        }
               
        //User и его создание админом
        public ActionResult CreateUser()
        {            
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUser(User user)
        {
            if (ModelState.IsValid)
            {
                dbUser.Create(user);
                dbUser.Save();
                return RedirectToAction("UserList");
            }
            else
            {
                ModelState.AddModelError("", "Некорректные данные");
            }
         return View(user);
        } 
        //User List 
        public ActionResult UserList()
        {
            return View(dbUser.GetList());
        }
        // GET: User/Edit/5

        [HttpGet]
        public ActionResult EditUser(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View();
        }      

        [HttpPost]
        public ActionResult EditUser(User user)
        {
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("AdminView");
        }
        // GET: User/Delete/5

        public ActionResult DeleteUser(int id)
        {
            User user = db.Users.Find(id);
            if (user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();
            }
            return View();
        }

       [HttpGet]
        public ActionResult DeleteUser(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View();
        }

        [HttpPost,ActionName("DeleteUser")]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("AdminView");
        }
        
        // Http Error
        private ActionResult HttpNotFound()
        {
            return View("Error");
        }      
    }
}
