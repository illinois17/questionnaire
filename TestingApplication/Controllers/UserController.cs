﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestingApplication.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestingApplication.Controllers
{
    public class UserController : Controller
    {
        IRepository<User> dbUser;
        IRepository<Test> dbTest;
        IRepository<Question> dbQuestion;
        IRepository<Choice> dbChoise;
        IRepository<Answer> dbAnswer;
        public static int buffer;
        public static int testNum;
        public static int userid;
        public UserController(MyDbContext context)
        {            
            dbUser = new UserRepository(context);
            dbTest = new TestRepository(context);
            dbQuestion = new QuestionRepository(context);
            dbChoise = new ChoiseRepository(context);
            dbAnswer = new AnswerRepository(context);
            userid = 1;// dbUser.GetList().Where(u=>u.Email== ).FirstOrDefault().ID;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult ChooseTest()
        {
            ViewBag.Tests = dbTest.GetList().ToList();
            return View();
        }
        [HttpGet]        
        public IActionResult Testing(int t)
        {
            testNum = t;
            buffer = 1;
            ViewBag.Question = dbQuestion.GetList().Where(q => q.TestID == t && q.QuestionNumber == buffer)?.FirstOrDefault();
            ViewBag.Choices = dbChoise.GetList().Where(c => c.TestID == t && c.QuestionNumber == buffer).ToList();
            return View();
        }
        [HttpPost]
        public IActionResult Testing(List<Answer> answers)
        {
            foreach (Answer ans in answers)
            {
                ans.UserID = userid;
                ans.TestID = testNum;
                ans.QuestionNumber = buffer;
                dbAnswer.Create(ans);
            }
            dbAnswer.Save();
            buffer++;
            if (buffer > dbQuestion.GetList().Where(q => q.TestID == 1).Count())
            {
                buffer = 1;
                return RedirectToAction("TestEnd");
            }
            ViewBag.Question = dbQuestion.GetList().Where(q => q.TestID == testNum && q.QuestionNumber==buffer).FirstOrDefault();
            ViewBag.Choices = dbChoise.GetList().Where(c => c.TestID == testNum && c.QuestionNumber == buffer).ToList();
            return View();
        }
        public ViewResult TestEnd()
        {       
            ViewBag.Answers = dbAnswer.GetList().Where(a => a.TestID == testNum && a.UserID == userid).ToList();
            return View(dbQuestion.GetList().Where(q => q.TestID == testNum).ToList());
        }


    }
}
