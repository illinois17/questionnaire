﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TestingApplication.Models
{
    public class Choice
    {

        public int ChoiceID { get; set; }
        public int TestID { get; set; }
        public int QuestionNumber { get; set; }
        [Required(ErrorMessage = "Введите текст ответа")]
        public string ChoiceText { get; set; }
        public bool IsCorrect { get; set; }

        public Question Question{get;set;}
        public int QuestionID { get; set; }
        public List<Answer> Answers { get; set; }
    }
   
}
